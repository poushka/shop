<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('register', 'AdminController@register');
Route::post('login', 'AdminController@login');
Route::get('getProducts', 'AppController@getAllProducts');


//Route::group(['prefix'=>"admin"], function() {
//    Route::post('login', 'AdminController@login');
//    //routes needing authentication
//    Route::group(['middleware'=>'adminAuth'],function() {
//        Route::post('register', 'AdminController@register');
//        Route::get('getAdmins', 'AdminController@getAdmins');
//        Route::get('adminPrivilages/{admin_id}', 'AdminController@adminPrivilages');
//
//        Route::post('privilage', 'AdminController@addPrivilage');
//        Route::delete('privilage/{admin_id}/{privilage_id}', 'AdminController@deletePrivilage');
//
//        //Category functions
//        Route::get('categories', 'CategoryControllerAdmin@getAll');
//        Route::post('categories', 'CategoryControllerAdmin@make');
//        Route::put('categories/{category}', 'CategoryControllerAdmin@update');
//        Route::delete('categories/{category}', 'CategoryControllerAdmin@delete');
//        Route::put('categoriesActivate/{category}', 'CategoryControllerAdmin@activation');
//
//    });
//
//});
//
//
//Route::post('Province', 'ProvinceContoroller@store');
//Route::get('Province', 'ProvinceContoroller@getAll');
//Route::get('categories', 'CategoryController@getAll');