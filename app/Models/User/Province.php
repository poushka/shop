<?php
namespace App\Models\User;
use App\City;
use Illuminate\Database\Eloquent\Model;

class Province extends Model {

    protected $table = 'province';

    protected $fillable=['name'];

    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany(City::class);
    }


}