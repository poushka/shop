<?php

namespace App\Http\Controllers;


use App\Admin;
use App\Http\Controllers\Controller;
use App\Models\User\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class CategoryControllerAdmin extends Controller
{
    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

    function getAll() {
        $categories= Category::all();
        return  $this->successReport($categories,"",200);
    }

    function make(Request $request) {
        $rules = ["name"=>"required|min:3"];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $request = $request->only('name');
       $result = Category::create($request);
       if (!$result) {
           return $this->failureResponse("خطا در ذخیره دسته",400);
       }
       return $this->successReport($result,"دسته جدید با موقفیت ساخته شد",201);
    }

    function update(Request $request,Category $category) {
        $rules = ["name"=>"required|min:3"];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $request = $request->only('name');
        $category->update($request);
        if ($category->wasChanged()) {
            return response()->json([],204);
        }else {
           return $this->failureResponse("خطا در به روز رسانی",400);
        }
    }

    function delete(Request $request,Category $category) {
        $result =  $category->delete();
        return response()->json([],204);

    }

    function activation(Request $request,Category $category) {
        $rules = ["active"=>"required|int|min:0|max:1"];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $request = $request->only('active');
        $category->update($request);
        if ($category->wasChanged()) {
            return response()->json([],204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }
    }


}
