<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminController extends Controller
{

    //registration
    function register(Request $request) {



        $rules = [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required',
            'password'=> 'required|min:6',

        ];
        $validator_message=[
            'password.min'=>'رمز عبور باید حداقل 6 کاراکتر باشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }




        $key = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));

        $admin=  User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password2' =>$request->get('password'),
                 'apikey'=>$key,

            ]);

            return $this->successReport($admin,"ثبت نام با موفیت انجام گردید",201);

    }




    function login(Request $request) {
        $rules = [
            'email' => 'required',
            'password'=> 'required'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $email = $request->get("email");
        $password =$request->get('password');



       $user = User::where([['email', $email],['password2',$password]])->first();

       if ($user==null) {
           return $this->failureResponse("ایمیل یا رمز عبور اشتباه است",401);
       }

       return $this->successReport($user,"ورود با موقثیت انجام شد",201);





    }








}
